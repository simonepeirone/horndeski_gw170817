!----------------------------------------------------------------------------------------
!
! This file is part of EFTCAMB.
!
! Copyright (C) 2013-2017 by the EFTCAMB authors
!
! The EFTCAMB code is free software;
! You can use it, redistribute it, and/or modify it under the terms
! of the GNU General Public License as published by the Free Software Foundation;
! either version 3 of the License, or (at your option) any later version.
! The full text of the license can be found in the file eftcamb/LICENSE at
! the top level of the EFTCAMB distribution.
!
!----------------------------------------------------------------------------------------

!> @file 04p9_darkenergy_parametrizations_1D.f90
!! This file contains the definition of the power law parametrization,
!! inheriting from parametrized_function_1D.


!----------------------------------------------------------------------------------------
!> This module contains the definition of the dark energy parametrization,
!! inheriting from parametrized_function_1D.

!> @author Bin Hu, Marco Raveri, Simone Peirone, Nelson Lima

module EFTCAMB_darkenergy_parametrizations_1D

    use precision
    use AMLutils
    use EFT_def
    use EFTCAMB_cache
    use EFTCAMB_abstract_parametrizations_1D
    use EFTCAMB_CPL_parametrizations_1D

    implicit none

    private

    public darkenergy_parametrization_1D

    ! ---------------------------------------------------------------------------------------------
    !> Type containing the power law function parametrization. Inherits from parametrized_function_1D.
    type, extends ( parametrized_function_1D ) :: darkenergy_parametrization_1D

        real(dl) :: coefficient

    contains
	
        ! utility functions:
        procedure :: set_param_number      => DarkEnergyParametrized1DSetParamNumber      !< subroutine that sets the number of parameters of the power law parametrized function.
        procedure :: init_parameters       => DarkEnergyParametrized1DInitParams          !< subroutine that initializes the function parameters based on the values found in an input array.
        procedure :: parameter_value       => DarkEnergyParametrized1DParameterValues     !< subroutine that returns the value of the function i-th parameter.
        procedure :: feedback              => DarkEnergyParametrized1DFeedback            !< subroutine that prints to screen the informations about the function.

        ! evaluation procedures:
        procedure :: value                 => DarkEnergyParametrized1DValue               !< function that returns the value of the power law function.
        procedure :: first_derivative      => DarkEnergyParametrized1DFirstDerivative     !< function that returns the first derivative of the power law function.
        procedure :: second_derivative     => DarkEnergyParametrized1DSecondDerivative    !< function that returns the second derivative of the power law function.
        procedure :: third_derivative      => DarkEnergyParametrized1DThirdDerivative     !< function that returns the third derivative of the power law function.
        procedure :: integral              => DarkEnergyParametrized1DIntegral            !< function that returns the strange integral that we need for w_DE.

    end type darkenergy_parametrization_1D

contains

    ! ---------------------------------------------------------------------------------------------
    ! Implementation of the power law function.
    ! ---------------------------------------------------------------------------------------------

    ! ---------------------------------------------------------------------------------------------
    !> Subroutine that sets the number of parameters of the power law parametrized function.
    subroutine darkenergyParametrized1DSetParamNumber( self )

        implicit none

        class(darkenergy_parametrization_1D) :: self       !< the base class

        ! initialize the number of parameters:
        self%parameter_number = 1



    end subroutine darkenergyParametrized1DSetParamNumber

    ! ---------------------------------------------------------------------------------------------
    !> Subroutine that initializes the function parameters based on the values found in an input array.
    subroutine darkenergyParametrized1DInitParams( self, array )

        implicit none

        class(darkenergy_parametrization_1D)                     :: self   !< the base class.
        real(dl), dimension(self%parameter_number), intent(in)  :: array  !< input array with the values of the parameters.

        self%coefficient = array(1)

    end subroutine darkenergyParametrized1DInitParams

    ! ---------------------------------------------------------------------------------------------
    !> Subroutine that returns the value of the function i-th parameter.
    subroutine darkenergyParametrized1DParameterValues( self, i, value )

        implicit none

        class(darkenergy_parametrization_1D) :: self        !< the base class
        integer     , intent(in)            :: i           !< The index of the parameter
        real(dl)    , intent(out)           :: value       !< the output value of the i-th parameter

        select case (i)
            case(1)
                value = self%coefficient
            case default
                write(*,*) 'Illegal index for parameter_names.'
                write(*,*) 'Maximum value is:', self%parameter_number
                call MpiStop('EFTCAMB error')
        end select

    end subroutine darkenergyParametrized1DParameterValues

    ! ---------------------------------------------------------------------------------------------
    !> Subroutine that prints to screen the informations about the function.
    subroutine darkenergyParametrized1DFeedback( self, print_params )

        implicit none

        class(darkenergy_parametrization_1D) :: self         !< the base class
        logical, optional                   :: print_params !< optional flag that decised whether to print numerical values
                                                            !! of the parameters.

        integer                             :: i
        real(dl)                            :: param_value
        character(len=EFT_names_max_length) :: param_name
        logical                             :: print_params_temp

        if ( present(print_params) ) then
            print_params_temp = print_params
        else
            print_params_temp = .True.
        end if

        write(*,*)     'Dark energy parametrization function: ', self%name
        if ( print_params_temp ) then
            do i=1, self%parameter_number
                call self%parameter_names( i, param_name  )
                call self%parameter_value( i, param_value )
                write(*,'(a23,a,F12.6)') param_name, '=', param_value
            end do
        end if

    end subroutine darkenergyParametrized1DFeedback

    ! ---------------------------------------------------------------------------------------------
    !> Function that returns the value of the function in the scale factor.
    function darkenergyParametrized1DValue( self, x, eft_cache )

        implicit none

        class(darkenergy_parametrization_1D)                :: self      !< the base class
        real(dl), intent(in)                               :: x         !< the input scale factor
        type(EFTCAMB_timestep_cache), intent(in), optional :: eft_cache !< the optional input EFTCAMB cache
        real(dl) :: darkenergyParametrized1DValue                         !< the output value

	darkenergyParametrized1DValue = self%coefficient

    end function darkenergyParametrized1DValue

    ! ---------------------------------------------------------------------------------------------
    !> Function that returns the value of the first derivative, wrt scale factor, of the function.
    function darkenergyParametrized1DFirstDerivative( self, x, eft_cache )

        implicit none

        class(darkenergy_parametrization_1D)                :: self      !< the base class
        real(dl), intent(in)                               :: x         !< the input scale factor
        type(EFTCAMB_timestep_cache), intent(in), optional :: eft_cache !< the optional input EFTCAMB cache
        real(dl) :: darkenergyParametrized1DFirstDerivative               !< the output value

        darkenergyParametrized1DFirstDerivative = self%coefficient

    end function darkenergyParametrized1DFirstDerivative

    ! ---------------------------------------------------------------------------------------------
    !> Function that returns the second derivative of the function.
    function darkenergyParametrized1DSecondDerivative( self, x, eft_cache )

        implicit none

        class(darkenergy_parametrization_1D)                :: self      !< the base class
        real(dl), intent(in)                               :: x         !< the input scale factor
        type(EFTCAMB_timestep_cache), intent(in), optional :: eft_cache !< the optional input EFTCAMB cache
        real(dl) :: darkenergyParametrized1DSecondDerivative              !< the output value

        darkenergyParametrized1DSecondDerivative = self%coefficient

    end function darkenergyParametrized1DSecondDerivative

    ! ---------------------------------------------------------------------------------------------
    !> Function that returns the third derivative of the function.
    function darkenergyParametrized1DThirdDerivative( self, x, eft_cache )

        implicit none

        class(darkenergy_parametrization_1D)                :: self      !< the base class
        real(dl), intent(in)                               :: x         !< the input scale factor
        type(EFTCAMB_timestep_cache), intent(in), optional :: eft_cache !< the optional input EFTCAMB cache
        real(dl) :: darkenergyParametrized1DThirdDerivative               !< the output value

        darkenergyParametrized1DThirdDerivative = self%coefficient


    end function darkenergyParametrized1DThirdDerivative

    ! ---------------------------------------------------------------------------------------------
    !> Function that returns the integral of the function, as defined in the notes.
    function darkenergyParametrized1DIntegral( self, x, eft_cache )

        implicit none

        class(darkenergy_parametrization_1D)                :: self      !< the base class
        real(dl), intent(in)                               :: x         !< the input scale factor
        type(EFTCAMB_timestep_cache), intent(in), optional :: eft_cache !< the optional input EFTCAMB cache
        real(dl) :: darkenergyParametrized1DIntegral                      !< the output value
	real(dl) :: w0, wa

        darkenergyParametrized1DIntegral = 0.0

    end function darkenergyParametrized1DIntegral

    ! ---------------------------------------------------------------------------------------------

end module EFTCAMB_darkenergy_parametrizations_1D

!----------------------------------------------------------------------------------------
