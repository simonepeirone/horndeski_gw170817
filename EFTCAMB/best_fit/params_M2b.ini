####### Mode selection flags for EFTCAMB  #######

#	 EFT flags: set up in which mode EFTCAMB is used.
# 	 We refer to the documentation (EFTCAMB:numerical notes) for a thorough
# 	 explanation of the effect of these flags.
#
# 1) Main EFT flag:
#      EFTflag = 0 : GR code. Every EFT modification is ignored.
#      EFTflag = 1 : Pure EFT code.
#      EFTflag = 2 : EFT alternative parametrizations.
#      EFTflag = 3 : designer mapping EFT.
#      EFTflag = 4 : full EFT mapping.

EFTflag = 1

# 2) Pure EFT model selection flag:
#      PureEFTmodel = 1 : standard pure EFT with 7 EFT functions and w_DE. Relevant if EFTflag = 1.

PureEFTmodel = 1

# 3) EFT alternative parametrizations model selection flag. Relevant if EFTflag = 2.
#
#      AltParEFTmodel = 1 : reparametrized Horndeski (RPH)

AltParEFTmodel = 1

# 4) Designer mapping EFT model selection flag. Relevant if EFTflag = 3.
#
#      DesignerEFTmodel = 1 : designer f(R)
#      DesignerEFTmodel = 2 : designer minimally coupled quintessence
#      DesignerEFTmodel = 3 : M2 model

DesignerEFTmodel = 3

# 5) Full mapping EFT model selection flag. Relevant if EFTflag = 4.
#
#      FullMappingEFTmodel = 1 : Horava gravity

FullMappingEFTmodel = 1

####### Model selection flags for EFTCAMB #######

# 1) Std. Pure EFT model selection flag.
#	 The following structure applies to every operator and can be specified for every operator
#	 separately. Relevant if EFTflag = 1 and PureEFTmodel = 1.
#
#      PureEFTmodel___ = 0 : Zero (operator ignored)
#      PureEFTmodel___ = 1 : Constant model
#      PureEFTmodel___ = 2 : Linear model
#      PureEFTmodel___ = 3 : Power law model
#      PureEFTmodel___ = 4 : Exponential model
#      PureEFTmodel___ = 5 : User defined

PureEFTmodelOmega  = 5
PureEFTmodelGamma1 = 5
PureEFTmodelGamma2 = 5
PureEFTmodelGamma3 = 0
PureEFTmodelGamma4 = 0
PureEFTmodelGamma5 = 0
PureEFTmodelGamma6 = 0

# 2) Pure EFT Horndeski: Restricts pure EFT models to Horndeski. Relevant if EFTflag = 1 and PureEFTmodel = 1.
#    Pure EFT choices for gamma_4, gamma_5, gamma_6 will be ignored and handled internally according to the Horndeski condition.

PureEFTHorndeski = F

# 3) w_DE model selection flag. Relevant for all the models where the expansion history 
#    can be choosen.
#      EFTwDE = 0 : Cosmological constant
#      EFTwDE = 1 : DE with constant Eos determined by EFTw0
#      EFTwDE = 2 : CPL parametrization
#      EFTwDE = 3 : JBP parametrization
#      EFTwDE = 4 : turning point parametrization
#      EFTwDE = 5 : Taylor expansion
#      EFTwDE = 6 : User defined

EFTwDE = 2

# 4) RPH model selection: select a model for the RPH functions. Relevant if EFTflag = 2 and AltParEFTmodel = 1.
#
#      RPH___model = 0 : Zero (function ignored)
#      RPH___model = 1 : Constant model
#      RPH___model = 2 : Linear model
#      RPH___model = 3 : Power Law model
#      RPH___model = 4 : User defined

RPHmassPmodel      = 0
RPHkineticitymodel = 0
RPHbraidingmodel   = 0
RPHtensormodel     = 0

# 5) Horava Solar System: Restricts Horava models to the sub-class which evades Solar System constraints.
#    Relevant only if EFTflag = 4 and FullMappingEFTmodel = 1.
#    Choices for Horava_xi will be automatically ignored.

HoravaSolarSystem = F

####### Stability conditions flags        #######

# 1) enforces the mathematical stability of the scalar field equation.
EFT_mathematical_stability = T

# 2) enforces the physical viability of the model.
EFT_physical_stability     = T

# 3) Additional priors on cosmological parameters. For more informations read the related section in the notes.
#    These conditions are model specific.
EFT_additional_priors        = T

####### Model parameters for EFTCAMB #######

#	Notice that if the model is not selected via the model selection flags then
#	the values of the parameters are automatically ignored.

#  1) Background Dark Energy equation of state parameters:

EFTw0  = -9.3691167E-01
EFTwa  = -3.1207107E-01


#  2) Pure EFT parameters:

EFTOmega0 =4.7421714E-02
EFTGamma10 =4.4289275
EFTGamma20 = -2.3197506E-01
