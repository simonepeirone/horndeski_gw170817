# KiDS-450

We include the modified CosmoMC modules and associated data files for weak lensing tomography cosmology fitting with KiDS in "kids450code.tar.gz".

We also include the modified CosmoMC modules and associated data above as part of a clean version of CosmoMC (July 2015 version) in "cosmomckids.tar.gz".

There is more specific information in the readme file included in the tarballs.

Please feel free to contact us at sjoudaki@swin.edu.au if you have any questions.
