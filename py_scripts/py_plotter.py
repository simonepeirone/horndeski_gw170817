import os, math, sys

here = os.path.dirname(os.path.abspath(__file__))
getdist_python_path = here+'/../python/'
sys.path.insert(0, os.path.normpath(getdist_python_path))

import getdist.plots as gplot
import getdist.chains as chains
import getdist.mcsamples
import getdist.densities as dens
import planckStyle
import numpy as np

cache = False

results_dir = here+'/res/'
chains_dir = here+'/../chains'

stat_results_dir = results_dir+'stats/'

analysis_settings = {'max_corr_2D': u'0.99',
		     'boundary_correction_order': u'0',
		     'converge_test_limit': u'0.95',
		     'smooth_scale_2D': u'0.3',
		     'credible_interval_threshold': u'0.05',
		     'contours': u'0.68 0.95 0.997',
		     'fine_bins_2D': u'256',
		     'num_bins': u'200',
		     'mult_bias_correction_order': u'0',
		     'fine_bins': u'1024',
		     'num_bins_2D': u'80',
		     'max_scatter_points': u'2000',
		     'range_ND_contour': u'0',
		     'range_confidence': u'0.001',
		     'smooth_scale_1D': u'0.3',
		     'ignore_rows': u'0.3'}

chains_roots = [
				'LCDM_kids',
                'CPL_kids',
				'CPL_kids_MS',
				'CPL_kids_PS',
				'LCDM_Planck',
				'CPL_Planck',
				'CPL_Planck_MS',
				'LCDM_Kids_Planck',
				'CPL_Kids_Planck',
				'CPL_Kids_Planck_MS'
                ]


# create the results folder if needed:
if not os.path.isdir(results_dir):
    os.mkdir(results_dir)

if not os.path.isdir(stat_results_dir):
    os.mkdir(stat_results_dir)

# get the marginal constraints without post-processing:
for root in chains_roots:
    # get the samples:
    sample = getdist.loadMCSamples(chains_dir+'/'+root, settings=analysis_settings, no_cache=cache)
    # digest input:
    sample.out_dir = stat_results_dir
    sample.rootdirname = os.path.join(stat_results_dir, root)

    # write covariance:
    sample.writeCovMatrix()
    # write correlation:
    sample.writeCorrelationMatrix()
    # write marginal statistics:
    sample.getMargeStats().saveAsText( sample.rootdirname + '.margestats' )

# post process and plot the relevant chains:
sample_1 = getdist.loadMCSamples(chains_dir+'/LCDM_kids', settings=analysis_settings, no_cache=cache)
sample_2 = getdist.loadMCSamples(chains_dir+'/CPL_kids', settings=analysis_settings, no_cache=cache)
sample_3 = getdist.loadMCSamples(chains_dir+'/CPL_kids_MS', settings=analysis_settings, no_cache=cache)
sample_4 = getdist.loadMCSamples(chains_dir+'/CPL_kids_PS', settings=analysis_settings, no_cache=cache)
sample_planck_LCDM 	 = getdist.loadMCSamples(chains_dir+'/LCDM_Planck', settings=analysis_settings, no_cache=cache)
sample_planck_CPL    = getdist.loadMCSamples(chains_dir+'/CPL_Planck', settings=analysis_settings, no_cache=cache)
sample_planck_CPL_MS = getdist.loadMCSamples(chains_dir+'/CPL_Planck_MS', settings=analysis_settings, no_cache=cache)
sample_kidsplanck_LCDM = getdist.loadMCSamples(chains_dir+'/LCDM_Kids_Planck', settings=analysis_settings, no_cache=cache)
sample_kidsplanck_CPL  = getdist.loadMCSamples(chains_dir+'/CPL_Kids_Planck', settings=analysis_settings, no_cache=cache)
sample_kidsplanck_CPL_MS  = getdist.loadMCSamples(chains_dir+'/CPL_Kids_Planck_MS', settings=analysis_settings, no_cache=cache)

# # test with the triangle before resampling:
# g1=gplot.getSubplotPlotter()
# params = [u'omegabh2',u'omegach2',u'theta',u'tau',u'logA',u'ns',u'sigma8']
# label=['LCDM', 'CPL']
# g1.add_legend(label,fontsize=10)
# g1.triangle_plot( [sample_1, sample_2] ,params, filled=True, shaded=False)
# g1.export(results_dir+'1_test_pre_kids.pdf')
#
# # test with the triangle before resampling:
# g1=gplot.getSubplotPlotter()
# params = [u'omegabh2',u'omegach2',u'theta',u'tau',u'logA',u'ns',u'sigma8']
# label=['LCDM', 'CPL']
# g1.add_legend(label,fontsize=10)
# g1.triangle_plot( [sample_planck_LCDM, sample_planck_CPL] ,params, filled=True, shaded=False)
# g1.export(results_dir+'1_test_pre_planck.pdf')
#
# # test with the triangle before resampling:
# g1=gplot.getSubplotPlotter()
# params = [u'omegabh2',u'omegach2',u'theta',u'tau',u'logA',u'ns',u'sigma8']
# label=['LCDM', 'CPL']
# g1.add_legend(label,fontsize=10)
# g1.triangle_plot( [sample_kidsplanck_LCDM, sample_kidsplanck_CPL] ,params, filled=True, shaded=False)
# g1.export(results_dir+'1_test_pre_kidsplanck.pdf')


# get the density of the first chain:
params_list = ['omegabh2', 'omegach2']
params_list = ['tau', 'logA']
params_list = ['omegabh2', 'omegach2', 'theta', 'tau', 'logA', 'ns']
par_numbers = [ sample_1.getParamNames().numberOfName(par) for par in params_list]

mean        = sample_1.getMeans()[par_numbers]
covariance  = sample_1.cov()[:,par_numbers][par_numbers,:]

inv_covariance = np.linalg.inv( covariance )

def gaussian( mean, x, inv_cov):
	return 0.1*np.dot(np.dot((mean-x),inv_cov),(mean-x))

par_numbers_1 = [ sample_1.getParamNames().numberOfName(par) for par in params_list]

new_weight = []
for weight, point in zip( sample_1.weights, sample_1.samples):
	x = point[par_numbers_1]
	new_weight.append( gaussian( mean, x, inv_covariance) )

new_weight = np.array( new_weight )
sample_1.reweightAddingLogLikes(new_weight)
sample_1.updateBaseStatistics()

# get the density of the second chain:
params_list = ['omegabh2', 'omegach2']
params_list = ['tau', 'logA']
params_list = ['omegabh2', 'omegach2', 'theta', 'tau', 'logA', 'ns']
par_numbers = [ sample_2.getParamNames().numberOfName(par) for par in params_list]

mean        = sample_2.getMeans()[par_numbers]
covariance  = sample_2.cov()[:,par_numbers][par_numbers,:]

inv_covariance = np.linalg.inv( covariance )

def gaussian( mean, x, inv_cov):
	return 0.1*np.dot(np.dot((mean-x),inv_cov),(mean-x))

par_numbers_2 = [ sample_2.getParamNames().numberOfName(par) for par in params_list]

new_weight = []
for weight, point in zip( sample_2.weights, sample_2.samples):
	x = point[par_numbers_2]
	new_weight.append( gaussian( mean, x, inv_covariance) )

new_weight = np.array( new_weight )
sample_2.reweightAddingLogLikes(new_weight)
sample_2.updateBaseStatistics()

# # test with the triangle after resampling:
# g2=gplot.getSubplotPlotter( )
# params = [u'omegabh2',u'omegach2',u'theta',u'tau',u'logA',u'ns',u'sigma8']
# label=['LCDM', 'CPL']
# g2.add_legend(label,fontsize=10)
# g2.triangle_plot( [sample_1, sample_2] ,params, filled=True, shaded=False)
# g2.export(results_dir+'2_test_post_kids.pdf')
#
# # test with the triangle after resampling:
# g2=gplot.getSubplotPlotter( )
# params = [u'omegabh2',u'omegach2',u'theta',u'tau',u'logA',u'ns',u'sigma8']
# label=['LCDM', 'CPL']
# g2.add_legend(label,fontsize=10)
# g2.triangle_plot( [sample_planck_LCDM, sample_planck_CPL] ,params, filled=True, shaded=False)
# g2.export(results_dir+'2_test_post_planck.pdf')
#
# # test with the triangle after resampling:
# g2=gplot.getSubplotPlotter( )
# params = [u'omegabh2',u'omegach2',u'theta',u'tau',u'logA',u'ns',u'sigma8']
# label=['LCDM', 'CPL']
# g2.add_legend(label,fontsize=10)
# g2.triangle_plot( [sample_kidsplanck_LCDM, sample_kidsplanck_CPL] ,params, filled=True, shaded=False)
# g2.export(results_dir+'2_test_post_kidsplanck.pdf')

# get the bounds:
sample_1.rootdirname = os.path.join(results_dir, 'LCDM_kids')
sample_2.rootdirname = os.path.join(results_dir,'CPL_kids')
sample_3.rootdirname = os.path.join(results_dir,'CPL_kids_MS')
sample_4.rootdirname = os.path.join(results_dir,'CPL_kids_PS' )
sample_planck_LCDM.rootdirname = os.path.join(results_dir,'LCDM_Planck' )
sample_planck_CPL.rootdirname = os.path.join(results_dir,'CPL_Planck' )
sample_planck_CPL_MS.rootdirname = os.path.join(results_dir,'CPL_Planck_MS' )
sample_kidsplanck_LCDM.rootdirname = os.path.join(results_dir,'LCDM_Kids_Planck' )
sample_kidsplanck_CPL.rootdirname = os.path.join(results_dir,'CPL_Kids_Planck' )
sample_kidsplanck_CPL_MS.rootdirname = os.path.join(results_dir,'CPL_Kids_Planck_MS' )

all_samples=[sample_1, sample_2, sample_3, sample_4, sample_planck_LCDM, sample_planck_CPL, sample_planck_CPL_MS, sample_kidsplanck_LCDM, sample_kidsplanck_CPL, sample_kidsplanck_CPL_MS]
for sample in all_samples:
	sample.out_dir = results_dir
	# write covariance:
	sample.writeCovMatrix()
	# write correlation:
	sample.writeCorrelationMatrix()
	# write marginal statistics:
	sample.getMargeStats().saveAsText( sample.rootdirname + '.margestats' )
	# write likelihood statistics:
	sample.getLikeStats().saveAsText( sample.rootdirname + '.likestat'    )

# g2=gplot.getSubplotPlotter( )

# # do the Plots sigma8 VS Omegam
# g2.newPlot()
# g2.plot_2d([sample_2, sample_1],   'omegam','sigma8', filled=True, lims=[  0.0,0.5,0.5,1.5])
# g2.add_legend(['CPL Full Stability - kids', 'LCDM - kids'], fontsize=5, legend_loc='best')
# g2.export(results_dir+'LCDM_vs_CPL_Full.pdf')
#
# g2.newPlot()
# g2.plot_2d([sample_3, sample_1],  'omegam','sigma8', filled=True, lims=[  0.0,0.5,0.5,1.5])
# g2.add_legend(['CPL Mathematical Stability - kids', 'LCDM - kids'], fontsize=5, legend_loc='best')
# g2.export(results_dir+'LCDM_vs_CPL_Math.pdf')
#
# g2.newPlot()
# g2.plot_2d([sample_4, sample_1],   'omegam','sigma8', filled=True, lims=[  0.0,0.5,0.5,1.5])
# g2.add_legend(['CPL Physical Stability - kids', 'LCDM - kids'], fontsize=5, legend_loc='best') #, colored_text=True)
# g2.export(results_dir+'LCDM_vs_CPL_Phys.pdf')
#
# g2.newPlot()
# g2.plot_2d([sample_4, sample_2],  'omegam',  'sigma8', filled=True, lims=[  0.0,0.5,0.5,1.5])
# g2.add_legend(['CPL Physical Stability - kids', 'CPL Full Stability - kids'], fontsize=5, legend_loc='best') #, colored_text=True)
# g2.export(results_dir+'CPL_Full_vs_Phys.pdf')
#
# # do the Plots planck
# g2.newPlot()
# g2.plot_2d([sample_planck_CPL, sample_planck_LCDM, sample_planck_CPL_MS],  'omegam', 'sigma8', filled=True, lims=[  0.0,0.5,0.5,1.5])
# g2.add_legend(['CPL Full Stabiligmaity - Planck', 'LCDM - Planck','CPL Mathematical Stability - Planck'], fontsize=5, legend_loc='best')
# g2.export(results_dir+'planck.pdf')
#
#
# # do the Plots w0 VS wa
# g2.newPlot()
# g2.plot_2d([ sample_3, sample_4, sample_2],  'EFTw0', 'EFTwa', filled=True, lims=[ -3,0, -5,4 ])
# g2.add_legend(['CPL - Mathematical Stability', 'CPL - Physical Stability', 'CPL - Full Stability'], fontsize=5, legend_loc='upper right') #, colored_text=True)
# g2.export(results_dir+'w0wa.pdf')
#
# g2.newPlot()
# g2.plot_2d([ sample_planck_CPL,sample_planck_CPL_MS],  'EFTw0', 'EFTwa', filled=True, lims=[ -3,0, -5,4 ])
# g2.add_legend(['CPL Full Stability - Planck','CPL Mathematical Stability - Planck'], fontsize=5, legend_loc='upper right') #, colored_text=True)
# g2.export(results_dir+'w0wa_planck.pdf')
#
# g2.newPlot()
# g2.plot_2d([ sample_kidsplanck_CPL,sample_kidsplanck_CPL_MS],  'EFTw0', 'EFTwa', filled=True, lims=[ -3,0, -5,4 ])
# g2.add_legend(['CPL','CPL Mathematical'], fontsize=5, legend_loc='upper right') #, colored_text=True)
# g2.export(results_dir+'w0wa_kids_planck.pdf')
#
#Plot only kids+Planck
g2 = gplot.getSinglePlotter(width_inch=4, ratio=0.7)
g2.plot_2d([ sample_planck_CPL,sample_planck_LCDM,sample_planck_CPL_MS], 'omegam', 'sigma8', filled=True, lims=[  0.0,0.5,0.5,1.5])
g2.add_legend(['CPL','LCDM','CPL math'], fontsize=10, legend_loc='best', colored_text=True)
g2.export(results_dir+'planck.pdf')
#
#Plot only LCDM
g2 = gplot.getSinglePlotter(width_inch=4, ratio=0.7)
g2.plot_2d([ sample_1,sample_planck_LCDM,],  'omegam', 'sigma8', filled=True, lims=[  0.0,0.5,0.5,1.5], colors=['gray', 'red'])
g2.add_legend(['KiDS','Planck 2015',], fontsize=10, legend_loc='best', colored_text=True)
g2.export(results_dir+'LCDM.pdf')
#
# #Plot only CPL
# g2.newPlot()
# g2.plot_2d([ sample_2,sample_planck_CPL,],  'omegam', 'sigma8', filled=True, lims=[  0.0,0.5,0.5,1.5])
# g2.add_legend(['Kids','Planck',], fontsize=5, legend_loc='best')
# g2.export(results_dir+'CPL_full.pdf')
#
#
# #Plot only CPL Mathematical
# g2.newPlot()
# g2.plot_2d([ sample_3,sample_planck_CPL_MS,],  'omegam', 'sigma8', filled=True, lims=[  0.0,0.5,0.5,1.5])
# g2.add_legend(['Kids','Planck',], fontsize=5, legend_loc='best')
# g2.export(results_dir+'CPL_Math.pdf')

# plot CPL vs LCDM
g2 = gplot.getSinglePlotter(width_inch=4, ratio=0.7)
g2.plot_2d([ sample_1,sample_planck_LCDM ,sample_2,sample_planck_CPL],  'omegam', 'sigma8',filled=[False, False,True, True], lims=[  0.05,0.5,0.5,1.5],colors=['gray','blue','green',  'red'])
g2.add_legend(['KiDS (LCDM)','Planck (LCDM)','KiDS (CPL)','Planck (CPL)'], fontsize=10, legend_loc='best', colored_text=True)
g2.export(results_dir+'CPLvsLCDM.pdf')

g3 = gplot.getSinglePlotter(width_inch=4, ratio=0.7)
g3.plot_2d([ sample_1,sample_planck_LCDM ,sample_3,sample_planck_CPL_MS],  'omegam', 'sigma8', filled=[False, False,True, True] , lims=[  0.05,0.5,0.5,1.5],colors=['gray','blue','green',  'red'])
g3.add_legend(['KiDS (LCDM)','Planck (LCDM)','KiDS (CPL math)','Planck (CPLmath)'], fontsize=10, legend_loc='best', colored_text=True)
# g3.GetDistPlotSettings(default_dash_styles)
g3.export(results_dir+'CPLmathvsLCDM.pdf')

#w0wa for CPL
g2 = gplot.getSinglePlotter(width_inch=4, ratio=0.7)
g2.plot_2d([ sample_2, sample_planck_CPL,sample_kidsplanck_CPL],  'EFTw0', 'EFTwa', filled=True,lims=[ -1.3,-0.2, -1,1 ], colors=['green',  'red', 'gray'])
g2.add_x_marker(-1.0)
g2.add_y_marker(0.0)
g2.add_legend(['KiDS','Planck 2015', 'KiDS + Planck'], fontsize=10, legend_loc='best', colored_text=True)
g2.export(results_dir+'w0wa_CPL.pdf')

#w0wa for CPL_MS
g2 = gplot.getSinglePlotter(width_inch=4, ratio=0.7)
g2.plot_2d([ sample_3, sample_planck_CPL_MS,sample_kidsplanck_CPL_MS],  'EFTw0', 'EFTwa', filled=True, lims=[ -3,0, -5,4 ],colors=['green',  'red', 'blue'], markers={'EFTw0':-1.0,'EFTwa':0.0 })
g2.add_x_marker(-1.0)
g2.add_y_marker(0.0)
g2.add_legend(['KiDS','Planck 2015', 'KiDS + Planck'], fontsize=10, legend_loc='best', colored_text=True)
g2.export(results_dir+'w0wa_CPLmath.pdf')


exit()
