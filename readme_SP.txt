This version of EFTCosmoMC comprehends:

-Latest EFTCAMB version;
-Latest CosmoMC base version with massive neutrino hierarchies as well as last BAO_DR12_consensus release;
-KiDS Likelihood with :
			-call to Weyl potential;
			-cut at non linear scales;
